# Enquetes_SQL_ps

Ceci est un dossier contenant les différents éléments afin de parvenir à la résolution d'enquêtes policières. Le but est de faire apprendre le language SQL à des élèves et étudiants de différents niveau.

Ce projet a été élaboré par les élèves Sébastien Cortes et Eya Jelassi, sous la direction de Mme Anne Laurent. 
Il a été repris par la suite par Clarence Rouvel.

## Prise de connaissances des différents fichiers du dossier 

#### Ici, chaque dossier et fichiers de ce répertoire seront explicités, afin de comprendre leur utilité et effectuer chaque étape convenablement.


## Prise en main de l'outil 

Vous devez accéder à un outil en ligne permettant d'éxécuter des requêtes en SQL pour le langage PostGreSQL.

https://www.crunchydata.com/developers/playground/psql-basics

(image capture d'écran)
Sur la partie gauche, un rappel de commandes basiques est fait. 
Ne vous inquiétez pas, les commandes que nous allons utiliser seront explicitées si vous ne voulez avoir à faire tout la lecture.

Sur la partie droite, c'est ici que nous allons travailler ! 
Donc retenez ces quelques points, avant que nous puissions démarrer !

### Lien afin de résoudre l'enquête 2 : https://www.crunchydata.com/developers/playground?sql=https://raw.githubusercontent.com/crouvel/Enquetes_SQL_ps/main/creation_remplissage_enquete2.sql

### Lien afin de résoudre l'enquête 3 : https://www.crunchydata.com/developers/playground?sql=https://raw.githubusercontent.com/crouvel/Enquetes_SQL_ps/main/creation_enquete3.sql


